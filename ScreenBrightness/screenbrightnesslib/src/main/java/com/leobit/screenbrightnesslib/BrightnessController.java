package com.leobit.screenbrightnesslib;

import android.app.Activity;
import android.content.ContentResolver;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

public class BrightnessController {
    private static String TAG = "BRIGHTNESS_TAG";

    public static void EnableManualBrightness(final Activity activity, final boolean isManual) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {
                    ContentResolver cResolver = activity.getContentResolver();


                    // To handle the auto
                    Settings.System.putInt(cResolver,
                            Settings.System.SCREEN_BRIGHTNESS_MODE,
                            isManual ? Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL : Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
                } catch (SecurityException e){
                    Log.e(TAG, "There is no permission in manifest or app isn't system.\n");
                    e.printStackTrace();
                }
            }
        });
    }

    /***
     *
     * @param brightness from 0 to 1
     */
    public static void SetBrightness(final Activity activity, final float brightness) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "trying to set brightness to " + brightness);

                WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
                lp.screenBrightness = brightness;
                activity.getWindow().setAttributes(lp);
            }
        });
    }

    public static float GetCurrentBrightness(Activity activity) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        float bright = lp.screenBrightness;
        Log.d(TAG, "trying to get brightness " + lp.screenBrightness + "  " + bright);
        return bright;
    }
}
