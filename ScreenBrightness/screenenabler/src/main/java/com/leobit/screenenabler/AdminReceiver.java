package com.leobit.screenenabler;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;

public class AdminReceiver extends DeviceAdminReceiver {

    public static final String ACTION_DISABLED = "device_admin_action_disabled";
    public static final String ACTION_ENABLED = "device_admin_action_enabled";

    @Override
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(ACTION_DISABLED));
    }
    @Override
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(ACTION_ENABLED));
    }

    public static void lock(Context context)
    {
        if (isScreenOn(context)) {
            DevicePolicyManager policy = (DevicePolicyManager)
                    context.getSystemService(Context.DEVICE_POLICY_SERVICE);
            try {
                //policy.lockNow();
            } catch (SecurityException ex) {

            }
        }
    }

    private static boolean isScreenOn(Context context)
    {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            if (pm.isInteractive()) {
                return true;
            }
        }
        else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH){
            if(pm.isScreenOn()){
                return true;
            }
        }

        else {
            return true;
        }
        return false;
    }
}
