package com.leobit.apprestarterlib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class RestarterReceiver extends BroadcastReceiver {
    private static String TAG = "SOLO_"+RestarterReceiver.class.getSimpleName();
    @Override
    public void onReceive(final Context context, Intent intent) {
        // create a handler to post messages to the main thread
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG+"_Broadcast Listened", "Service tried to stop");
                //Toast.makeText(context, "Service restarted", Toast.LENGTH_SHORT).show();

                context.startService(new Intent(context, RestarterService.class));
            }
        });
    }
}
