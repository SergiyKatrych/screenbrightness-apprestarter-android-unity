package com.leobit.apprestarterlib;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Debug;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RestarterService extends Service {
    public int counter=0;
    private static String TAG = "SOLO_"+RestarterService.class.getSimpleName();

    final public static String USER_STOP_SERVICE_REQUEST = "USER_STOP_SERVICE";
    final public static String SHOULD_RESTART = "SHOULD_RESTART";


    private boolean shouldRestart= true;

    public static String PACKAGE_NAME;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            shouldRestart = intent.getBooleanExtra(SHOULD_RESTART, false);
            Log.d(TAG, "Broadcast receiver received should restart "+shouldRestart);

            Intent restarterIntent = new Intent(context, RestarterService.class);
            context.stopService(restarterIntent);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(1, new Notification());
        PACKAGE_NAME = getApplicationContext().getPackageName();
        Log.d(TAG,"Package name is "+PACKAGE_NAME);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                receiver, new IntentFilter(USER_STOP_SERVICE_REQUEST));
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stoptimertask();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        if (shouldRestart){
            Log.e(TAG, "onDestroy, trying to restart service");
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("restartservice");
            broadcastIntent.setClass(this, RestarterReceiver.class);
            this.sendBroadcast(broadcastIntent);
        }
    }



    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                Log.d(TAG, "timer ticking, app is running "+Helper.isAppRunning(getBaseContext(), PACKAGE_NAME));
                if (counter==Integer.MAX_VALUE) counter = 0;

                if (Helper.isAppRunning(getBaseContext(), PACKAGE_NAME)) {
                    // App is running - do nothing
                } else {
                    // App is not running
                    Log.d(TAG, "APP IS NOT RUNNING!! ACHTUNG!! Restarting app in 60 seconds");
                    Helper.RestartApplication(60, getBaseContext());
                }
            }
        };
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        Log.e(TAG, "Retarter service stopped");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
