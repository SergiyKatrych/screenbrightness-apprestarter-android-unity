package com.leobit.apprestarterlib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class OnUpgradeReceiver extends BroadcastReceiver {
    private static String TAG = "SOLO_"+OnUpgradeReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("AppLog", "current app was upgraded");
        Helper.StartRestarterService(context);
    }
}
