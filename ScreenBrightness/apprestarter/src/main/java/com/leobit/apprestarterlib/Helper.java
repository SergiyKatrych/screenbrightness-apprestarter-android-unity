package com.leobit.apprestarterlib;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;

public class Helper {

    private static String TAG = "SOLO_"+Helper.class.getSimpleName();
    private static boolean isAppRestarting = false;


    public static boolean isAppRunning(final Context context, final String packageName) {

        //if device is locked - we cannot restart app. will be waiting till device will be re-unlocked
        if (IsDeviceLocked(context)) {
            return true;
        }

        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void StopRestarterService(Context context){

        Intent intent = new Intent(RestarterService.USER_STOP_SERVICE_REQUEST);
        intent.putExtra(RestarterService.SHOULD_RESTART, false);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        Log.d(TAG, "try to stop service");
    }

    public static void StartRestarterService(Context context){
        if (!isMyServiceRunning(context, RestarterService.class)) {
            Log.d(TAG, "try to start service");
            Intent restarterIntent = new Intent(context, RestarterService.class);
//            if (Build.VERSION.SDK_INT >= 25) {
//                context.startForegroundService(restarterIntent);
//            } else {
                context.startService(restarterIntent);
//            }
        }
    }

    private static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                //Log.i (TAG+"_Service status", "Running");
                return true;
            }
        }
        Log.i (TAG+"_Service status", "Not running");
        return false;
    }


    public static void RestartApplication(int delay, Context context) {
        //ignore if we are already restarting
        if (isAppRestarting) return;
        isAppRestarting = true;

        StopRestarterService(context);


        if (delay == 0) {
            delay = 1;
        }

        try{
            Log.e(TAG, "restarting app in "+delay+" seconds. context "+context);
            Log.e(TAG, "Step 0");
            Intent restartIntent = context.getPackageManager()
                    .getLaunchIntentForPackage(context.getPackageName() );

            Log.e(TAG, "Step 1");
            restartIntent.putExtra("crash", true);
            restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);

            Log.e(TAG, "Step 2");

            PendingIntent intent = PendingIntent.getActivity(context, 0,
                    restartIntent, PendingIntent.FLAG_ONE_SHOT);

            Log.e(TAG, "Step 3");
            AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Log.e(TAG, "Step 4");
            manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay*1000, intent);

            Log.e(TAG, "Step 5");

            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);

            System.exit(0);
        }
        catch (Exception e) {
            Log.e(TAG, "Smthng happened here "+e);
            StartApplication(context);
        }
    }

    public static void RemoveAlarmIntent(Context context)
    {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);


        Intent restartIntent = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName() );

        Log.e(TAG, "Step 1");
        restartIntent.putExtra("crash", true);
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);

        Log.e(TAG, "Step 2");

        PendingIntent intent = PendingIntent.getActivity(context, 0,
                restartIntent, PendingIntent.FLAG_ONE_SHOT);

        alarmManager.cancel(intent);
    }

    private static void StartApplication(Context context)
    {
        Intent dialogIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(dialogIntent);
    }

    private static boolean IsDeviceLocked(Context context) {
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

        return km.inKeyguardRestrictedInputMode();
    }
}
